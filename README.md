# Réinitialiser Windows 8
**Je vous invite à lire l'intégralité du tuto avant de vous lancer tête baissé dans la réinitialisation de votre PC.
Il peut y avoir des subtilités qui risquent d'apparaitre plus tard.**

## Introduction
Avant de réinitialiser Microsoft Windows 8, vous allez devoir :
- Sauvegarder vos données (optionnel, recommendé)
- Récupérer votre clé de license Windows 8 (optionnel, recommandé)

Pour faire ceci vous aurez besoin:
- D'un disque dur ou d'un espace cloud pour sauvegarder vos données
- Un bout de papier pour écrire la clé de votre license Microsoft Windows 8

## Sauvegarder ses données
### Avec un disque dur
1. Branchez le disque dur
	En branchant votre disque dur sur votre ordinateur vous avez accès à son système de fichier (son contenu).
	Vous pouvez faire un **clic droit** sur son nom et regarder dans les **propriétés** pour trouver son espace libre.

2. Copier les fichiers
	Vous devez glisser-déposer tous les fichiers que vous souhaitez conserver à l'intérieur.
	Vous pouvez créer un dossier regroupant tous les fichiers pour aller plus vite.
	Cette étape peut prendre quelques temps.
	
3. Ejecter le disque dur
	Une fois tous les fichiers transféré sur votre disque dur pensez à l'éjecter.
	Pour ça il faut appuyer sur le petit rond vert en bas à droite de l'écran.
	
### Sur un service de cloud
Cette méthode dépend de votre fournisseur de cloud, cependant on retrouve souvent un fonctionnement similaire à l'étape 2 de la sauvegarde de données sur un disque dur.

## Récupérer la clé de license Windows
Normalement cette clé est écrite dans un espace mémoire protégé. 
Il n'est donc pas nécéssaire dans la majorité des cas de le noter. 
Cependant nous allons tout de même le faire. **Au cas où**
Vous pouvez télécharger et installer [ProduKey](http://www.nirsoft.net/utils/produkey-x64.zip "Télécharger ProduKey")
Ce logiciel permet de récupérer les clés de license des outils de Microsoft, notamment Windows et la suite Office.
Notez les si vous voulez les conserver, ça peut toujours servir en cas de pépin.
Si vous préférez le faire à la main vous pouvez vous renseignez sur les registres de Windows, cependant je ne vous aiderai pas pour ça.
Vous devez aussi vous renseignez sur comment récupérer les clés de license des différents logiciels propriétaires que vous utilisez.

## Réinitialiser Windows 8.1
### Réinitialisation
1. Balayez l’écran à partir du bord droit et appuyez sur **Paramètres**, puis sur **Modifier les paramètres du PC**.
(Si vous utilisez une souris, pointez sur le coin supérieur droit de l’écran, déplacez le pointeur de la souris vers le bas et cliquez sur Paramètres, puis sur Modifier les paramètres du PC.)
2. Appuyez ou cliquez sur **Mise à jour et récupération**, puis sur **Récupération**. 
3. Sous **Tout supprimer et réinstaller Windows**, appuyez ou cliquez sur **Commencer**. 
4. Suivez les instructions qui s’affichent à l’écran.

### Remarque
Vous devez indiquer si vous souhaitez effectuer un effacement rapide ou minutieux des données. Si vous choisissez d’effacer rapidement les données, certaines d’entre elles sont éventuellement récupérables à l’aide d’un logiciel spécialisé. Si vous préférez les effacer minutieusement, l’opération prend plus de temps mais la récupération des données est moins probable.


